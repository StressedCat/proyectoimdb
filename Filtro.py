#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from Rating import FiltroRating
from Fecha import FechaFiltro
from Title import NameFiltro


class Filtracion():
    """
        Se guardara la informacion basica de nuestro filtro y será usada
        cuando se necesite.

        Al mismo tiempo se usara para actualizar nuestros filtros en caso de
        que sean cambiados por medio de 'x'Filter
    """
    def __init__(self, TS, YS, RU, RC, M, D):
        self.Title = TS
        self.Year = YS
        self.Ruser = RU
        self.Rcrit = RC
        self.Month = M
        self.Day = D

    def DateFilter(self, c1, c2, c3):
        self.Year = c1.Anio
        self.Month = c2.Mes
        self.Day = c3.Dia

    def RateFilter(self, c1, c2):
        self.Ruser = c1.Usuarios
        self.Rcrit = c2.Criticos

    def TitleFilter(self, c1):
        self.Title = c1.Titulo
