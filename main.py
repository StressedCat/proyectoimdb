#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import pandas as pd
from Filtro import Filtracion
from Rating import FiltroRating
from Fecha import FechaFiltro
from Title import NameFiltro


def OpenScv():
    data = pd.read_csv("IMDb movies.csv", error_bad_lines=False)
    print(data.columns)
    return data


def TituloSearch(Filtro):
    """
        se buscara cual pelicula se desea por el nombre, sin importar sus
        mayusculas ya que al buscarla, todo se ingresara con mayusculas para
        buscar lo que se deseaba
    """
    print("¿Como se llama su pelicula?")
    TS = input("Ingrese el nombre: ")
    TSF = NameFiltro(TS)
    Filtro.TitleFilter(TSF)
    return


def RatingSearch(Filtro):
    """
        se hara la filtracion segun las reviews de los usuarios y criticos,
        en el caso que solo se quiera una, se puede dejar vacia la otra
        y sera tomada como un None el cual no será buscada en el filtro
    """
    print("¿Cuanto es el rating")
    print("En caso de no querer uno, ingrese nada")
    print("El formato es: numero.decima")
    RU = input("Ingrese su rating de usuarios: ")
    RC = input("Ingrese el rating de los criticos: ")
    if RU == "":
        RU = None
    else:
        RU = float(RU)
        RU = round(RU, 1)
    if RC == "":
        RC = None
    else:
        RC = float(RC)
        RC = round(RC, 1)
    FR = FiltroRating(RU, RC)
    Filtro.RateFilter(FR, FR)
    return


def YearSearch(Filtro):
    """
        solo se usara el año en este caso, donde el usuario ingresara el año
        pero no el mes y el dia para filtrar
    """
    print("¿En que año fue creado?")
    Y = int(input("Ingrese el año: "))
    M = None
    D = None
    FD = FechaFiltro(Y, M, D)
    Filtro.DateFilter(FD, FD, FD)
    return


def DateSearch(Filtro):
    """
        Se creara una fecha para filtrar las peliculas ingresada
        por el usuario
    """
    print("¿Que fecha fue creada?")
    Y = input("Ingrese el año: ")
    M = input("Ingrese el mes: ")
    D = input("Ingrese el dia: ")
    FD = FechaFiltro(Y, M, D)
    Filtro.DateFilter(FD, FD, FD)
    return


def MakeDoc(Movies, Filtro):
    """
        este es el paso final, donde se realizara la filtracion y será
        mostrada para el usuario, al final se tiene una opcion donde el
        usuario puede transformar el filtro que tenia en un archivo scvs
        el cual mostrará su descripcion, genero y que fecha se publico
        exactamente
    """
    FilMov = Movies.filter(["title",
                            "year",
                            "date_published",
                            "genre",
                            "description",
                            "reviews_from_users",
                            "reviews_from_critics"])
    if Filtro.Year is not None and Filtro.Month is not None:
        FechaFinal = str("{}-{}-{}".format(Filtro.Year,
                                           Filtro.Month,
                                           Filtro.Day))
        FilMov = FilMov[FilMov["date_published"] == FechaFinal]
    elif Filtro.Year is not None:
        FilMov = FilMov[FilMov["year"] == Filtro.Year]
    if Filtro.Ruser is not None:
        FilMov = FilMov[FilMov["reviews_from_users"] == Filtro.Ruser]
    if Filtro.Rcrit is not None:
        FilMov = FilMov[FilMov["reviews_from_critics"] == Filtro.Rcrit]
    if Filtro.Title is not None:
        FilMov = FilMov[FilMov["title"].str.upper() == Filtro.Title.upper()]
    print(FilMov)
    print("\nPresione Y si desea descargarlo en un archivo")
    print("Al realizar esto, recibira informacion mas completa")
    print("Caso contrario, presione cualquier otro boton")
    Choice = input("Ingrese su eleccion: ")
    if Choice.upper() == "Y":
        FilMov.to_csv("ArchivoDeseado.csv", sep=';', index=False)
        pass


def Base(Movies):
    choice = "a"
    Filtro = Filtracion(None, None, None, None, None, None)
    """
        Se creara el menu donde se realizaran todo los tipos de filtracion
        cada uno con su posibilidad de "resetear" para borrar los datos que
        anteriormente se habian ingresado
    """
    while choice.upper() != "X":
        print("\n")
        print("Buenas tardes, que operacion realizará para filtrar")
        print("S para filtrar por titulo de la pelicula")
        print("SR para resetear el titulo de la pelicula")
        print("R para filtrar con el Rating")
        print("RR para resetear el Rating")
        print("Y para filtrar con el año")
        print("D para filtrar con fecha de lanzamiento")
        print("DR para resetear la fecha")
        print("E para terminar su busqueda y realizar filtracion")
        print("X para salir")
        choice = input("Haga su eleccion: \n")
        if choice.upper() == "S":
            TituloSearch(Filtro)
        elif choice.upper() == "R":
            RatingSearch(Filtro)
        elif choice.upper() == "D":
            DateSearch(Filtro)
        elif choice.upper() == "Y":
            YearSearch(Filtro)
        elif choice.upper() == "E":
            MakeDoc(Movies, Filtro)
        elif choice.upper() == "DR":
            Reset = FechaFiltro(None, None, None)
            Filtro.DateFilter(Reset, Reset, Reset)
        elif choice.upper() == "RR":
            Reset = FiltroRating(None, None)
            Filtro.RateFilter(Reset, Reset)
        elif choice.upper() == "SR":
            Reset = NameFiltro(None)
            Filtro.TitleFilter(Reset)
        elif choice.upper() == "X":
            quit()
        else:
            print("Eleccion erronea, intente de nuevo")


if __name__ == '__main__':
    """
        se inicia todo con abrir el scv, el cual sera realizado con pandas
        despues de esto entrara al menu donde se realizaran las opciones
    """
    Movies = OpenScv()
    Base(Movies)
